# -*- mode:sh -*-

REPO_ROOT=${REPO_ROOT:-~/repos}

bitbucket-hg-clone() {
#1: bb_user/repo_name
#2: repo_name
  path=$REPO_ROOT/${2:-$(basename $1)}
  hg clone ssh://bitbucket.org/$1 $path
  cd $path
}

bitbucket-hg-get() {
#1: bb_user
#2: repo_name
  user=$1
  name=$2
  path=$REPO_ROOT/$name
  echo $path

  if [ ! -d $path ]; then
    bitbucket-hg-clone $user/$name
    return
  fi

  if [ ! -d $path/.hg ]; then
    echo "Error: '$path' exists but it is not a mercurial repo."
    return
  fi

  cd "$path"
  if [ "$3" = "-u" ]; then
    hg pull -u
  fi
}

svn2bitbucket() {
#1: bb_user
#2: old svn_uri
#3: new repo_name
  orig=$(basename $2)
  echo $orig default > /tmp/branchmap
  hg convert --branchmap /tmp/branchmap $2 $3
  cat > $3/.hg/hgrc << EOF
[paths]
default = ssh://bitbucket.org/$1/$3
EOF
  hg --cwd $3 push
}
